﻿using Eiffel240.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Eiffel240.Repository;

namespace Eiffel240.Controllers
{
    public class DetailsController : Controller
    {
        // GET: Details
        public ActionResult Index()
        {
            return PartialView();
        }

        public ActionResult List()
        {
            return PartialView("_List");
        }
    }
}