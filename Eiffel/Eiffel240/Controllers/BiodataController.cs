﻿using Eiffel.DataModel;
using Eiffel240.DataModel;
using Eiffel240.Repository;
using Eiffel240.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Eiffel240.Controllers
{
    public class BiodataController : Controller
    {
        // GET: Biodata
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult List(long id)
        {
            ViewBag.jeniskelamin = BiodataRepo.genderName(id);
            List<BiodataViewModel> list = BiodataRepo.AllList(id);
            return PartialView("_List", list);
        }

        public ActionResult ListLama(long id)
        {
            ViewBag.agama = BiodataRepo.religionName();
            ViewBag.identitas = BiodataRepo.identityName();
            ViewBag.status = BiodataRepo.statusName();
            //ViewBag.gender = BiodataRepo.genderName();
            List<x_biodata> list = BiodataRepo.AllById(id);
            return PartialView("_List", list);
        }

        public static List<SelectListItem> tahunList()
        {
            List<SelectListItem> tahunList = new List<SelectListItem>();
            int tahun = DateTime.Now.Year;
            for (int x = tahun; x >= 1950; x--)
            {
                tahunList.Add(new SelectListItem { Value = x.ToString() });
            }
            return tahunList;
        }

        public ActionResult Edit(long id)
        {
            //BUAT DI FORM
            ViewBag.agamaList = new SelectList(ReligionRepo.All(), "id", "name");
            ViewBag.identitasList = new SelectList(IdentityTypeRepo.All(), "id", "name");
            ViewBag.statusList = new SelectList(MaritalStatusRepo.All(), "id", "name");
            ViewBag.genderList = new SelectList(BiodataRepo.genderName(id), "Value", "Text");
            ViewBag.tahunList = new SelectList(tahunList(), "Value", "Value");

            //if (ViewBag.statusList.name == "Lajang")
            //{
            //    ViewBag.tahunList.add("disabled", "disabled");
            //}

            return PartialView("_Edit", BiodataRepo.ByIdViewModel(id));
        }

        [HttpPost]
        public ActionResult Edit(BiodataViewModel model)
        {
            try
            {
                ResponseResult result = BiodataRepo.Update(model);
                BiodataRepo.alert = string.Empty;

                //VALIDASI ANAK KE- DENGAN JUMLAH SAUDARA
                //int[] angka = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
                int anakKe = Convert.ToInt32(model.child_sequence);
                int totalsaudara = Convert.ToInt32(model.how_many_brothers);
                //Validasi jumlah
                if (anakKe > totalsaudara)
                {
                    BiodataRepo.alert = "Jumlah 'Anak-Ke' atau 'Jumlah Saudara' salah.";
                }
                //Validasi Inputan angka
                string[] anak = new string[] { model.child_sequence };
                string[] sodara = new string[] { model.how_many_brothers };
                if (anak.Length > 1 || sodara.Length > 1)
                {
                    BiodataRepo.alert = "Format penulisan 'Anak-Ke' atau 'Jumlah Saudara' salah.";
                }                

                //VALIDASI TINGGI DAN BERAT
                int tinggi = Convert.ToInt32(model.high);
                int berat = Convert.ToInt32(model.weight);
                if (tinggi > 250)
                {
                    BiodataRepo.alert = "Format tinggi salah.";
                }
                if (berat > 200)
                {
                    BiodataRepo.alert = "Format berat salah.";
                }

                //VALIDASI EMAIL
                String[] val_email = model.email.Split('@');
                if (val_email.Length == 1)
                {
                    BiodataRepo.alert = "Format email salah. ";
                }

                //VALIDASI NO.HP
                char[] hp = model.phone_number1.ToCharArray();
                char[] php = model.parent_phone_number.ToCharArray();
                if (model.phone_number2 != null) //Jika ada
                {
                    char[] ahp = model.phone_number2.ToCharArray();
                    for (int j = 0; j < ahp.Length; j++)
                    {
                        if (ahp[0] != '0' && ahp[1] != '8')
                        {
                            BiodataRepo.alert = "Format No. HP salah.";
                        }
                    }
                }
                for (int i = 0; i < 2; i++)
                {
                    if (hp[0] != '0' && hp[1] != '8' ||
                        php[0] != '0' && php[1] != '8')
                    {
                        BiodataRepo.alert = "Format No. HP salah.";
                        break;
                    }
                }

                using (var db = new Eiffel240Context())
                {
                    var data = db.x_biodata.ToList();
                    //VALIDASI EMAIL DENGAN DATABASE
                    if (data.Any(o => o.email == model.email && o.id != model.id))
                    {
                        BiodataRepo.alert = "Email sudah pernah digunakan.";
                    }
                    //VALIDASI NO. HP DENGAN DATABASE
                    if (data.Any(o => o.phone_number1 == model.phone_number1 && o.id != model.id))
                    {
                        BiodataRepo.alert = "No. HP sudah pernah digunakan.";
                    }
                    //VALIDASI IDENTITAS DENGAN DATABASE
                    if (data.Any(o => o.identity_type_id == model.identity_type_id && o.identity_no == model.identity_no && o.id != model.id))
                    {
                        BiodataRepo.alert = "Identitas sudah pernah digunakan.";
                    }
                }

                if (string.IsNullOrEmpty(BiodataRepo.alert))
                {
                    return Json(
                    new
                    {
                        success = BiodataRepo.Update(model),
                        message = result.Message,
                    },
                    JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(
                    new
                    {
                        success = false,
                        message = BiodataRepo.alert,
                    },
                    JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(
                    new
                    {
                        success = false,
                        message = ex.Message,
                    },
                    JsonRequestBehavior.AllowGet);
            }
            
        }
    }
}


//List<x_address> ala = new List<x_address>();
//List<x_biodata> bio = new List<x_biodata>();
//List<string> result = new List<string>();
//using (var db = new Eiffel240Context())
//{
//    ala = db.x_address.ToList();
//    for (int i = 0; i < ala.Count; i++)
//    {
//        for (int j = 0; j < bio.Count; j++)
//        {
//            if (ala[i].biodata_id == bio[j].id)
//            {
//                ViewBag.alamat1 = ala[i].address1;
//                ViewBag.pos1 = ala[i].postal_code1;
//                ViewBag.rt1 = ala[i].rt1;
//                ViewBag.rw1 = ala[i].rw1;
//                ViewBag.kelurahan1 = ala[i].kelurahan1;
//                ViewBag.kecamatan1 = ala[i].kecamatan1;
//                ViewBag.kota1 = ala[i].region1;
//                ViewBag.alamat2 = ala[i].address2;
//                ViewBag.pos2 = ala[i].postal_code2;
//                ViewBag.rt2 = ala[i].rt2;
//                ViewBag.rw2 = ala[i].rw2;
//                ViewBag.kelurahan2 = ala[i].kelurahan2;
//                ViewBag.kecamatan2 = ala[i].kecamatan2;
//                ViewBag.kota2 = ala[i].region2;
//            }
//        }
//    }
//}