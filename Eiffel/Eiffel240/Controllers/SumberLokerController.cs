﻿using Eiffel.DataModel;
using Eiffel240.DataModel;
using Eiffel240.Repository;
using Eiffel240.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Eiffel240.Controllers
{
    public class SumberLokerController : Controller
    {
        // GET: SumberLoker
        public ActionResult Index()
        {
            return View();
        }

        public static List<SelectListItem> sumberLowongan()
        {
            List<SelectListItem> namaSumber = new List<SelectListItem>();
            List<x_biodata> bio = new List<x_biodata>();
            List<x_vacancy_source> vac = new List<x_vacancy_source>();
            List<x_sumber_loker> lok = new List<x_sumber_loker>();
            using (var db = new Eiffel240Context())
            {
                bio = db.x_biodata.ToList();
                vac = db.x_vacancy_source.ToList();
                lok = db.x_sumber_loker.ToList();
                for (int i = 0; i < vac.Count; i++)
                {
                    namaSumber.Add(new SelectListItem { Value = vac[i].name });
                }
            }
            return namaSumber;
        }


        public ActionResult Edit(long id)
        {
            ViewBag.sourceList = new SelectList(sumberLowongan(), "value", "value");
            return PartialView("_Edit", SumberLokerRepo.ByIdViewModel(id));
        }

        [HttpPost]
        public ActionResult Edit(SumberLokerViewModel model)
        {
            try
            {
                ResponseResult result = SumberLokerRepo.Update(model);
                SumberLokerRepo.alert = string.Empty;

                if (model.vacancy_source == "Referensi")
                {
                    if (model.referrer_name == null || model.referrer_phone == null || model.referrer_email == null) 
                    {
                        SumberLokerRepo.alert = "Field tidak boleh kosong!";
                        goto cek;
                    }
                }

                ////VALIDASI PILIHAN SUMBER
                //if (model.vacancy_source == null || model.candidate_type == null)
                //{
                //    SumberLokerRepo.alert = "Pilihan Sumber atau Tipe Kandidat tidak boleh kosong!";
                //}

                //VALIDASI EMAIL
                String[] val_email = model.referrer_email.Split('@');
                if (val_email.Length == 1)
                {
                    SumberLokerRepo.alert = "Format email salah. ";
                }

                //VALIDASI NO.HP
                char[] rhp = model.referrer_phone.ToCharArray();
                if (rhp[0] != '0' && rhp[1] != '8')
                {
                    SumberLokerRepo.alert = "Format No. HP salah.";
                }

                cek:
                if (string.IsNullOrEmpty(SumberLokerRepo.alert))
                {
                    return Json(
                    new
                    {
                        success = SumberLokerRepo.Update(model),
                        message = result.Message,
                    },
                    JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(
                    new
                    {
                        success = false,
                        message = SumberLokerRepo.alert,
                    },
                    JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(
                new
                {
                    success = false,
                    message = "salah",
                },
                JsonRequestBehavior.AllowGet);
            }                        
        }
    }
}