﻿using Eiffel240.DataModel;
using Eiffel240.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Eiffel240.Controllers
{
    public class PelamarController : Controller
    {
        // GET: Pelamar
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult List()
        {
            List<x_biodata> list = BiodataRepo.All();
            return PartialView("_List", list);
        }
    }
}